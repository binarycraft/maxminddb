const std = @import("std");
const maxminddb = @import("maxminddb");
const print = std.debug.print;

pub fn main() !void {
    const allocator = std.heap.page_allocator;
    var mmdb = try allocator.alloc(maxminddb.MMDB_s, 1);
    defer allocator.free(mmdb);

    var mmdb_name = "GeoLite2-Country.mmdb";
    var status = maxminddb.MMDB_open(
        mmdb_name,
        maxminddb.MMDB_MODE_MMAP,
        mmdb.ptr,
    );
    defer maxminddb.MMDB_close(mmdb.ptr);

    if (maxminddb.MMDB_SUCCESS != status) {
        print(
            "Failed to open mmdb: {s}\n",
            .{mmdb_name},
        );
    }
}
